# Ban messages from channels in your chat

## Info

This project is for banning messages from channels in telegram chats

![](working.png)


## Installing

1. Copy: git clone https://gitlab.com/SPUZ_/telegram_channels_ban
2. Build: docker build -t telegram_channels_ban .
3. Set bot token with env TOKEN
4. Done
